CREATE SEQUENCE  user_id;
CREATE TABLE IF NOT EXISTS users (
    id integer PRIMARY KEY DEFAULT nextval('user_id') ,
    username varchar(255) UNIQUE,
    email varchar(255) UNIQUE,
    password varchar(255)
);
ALTER SEQUENCE user_id OWNED BY users.id;

CREATE SEQUENCE  gif_id;
CREATE TABLE IF NOT EXISTS gifs (
    id integer PRIMARY KEY DEFAULT nextval('gif_id') ,
    user_id integer references users(id),
    filename varchar(255) UNIQUE,
    description TEXT
);
ALTER SEQUENCE gif_id OWNED BY gifs.id;

CREATE SEQUENCE  like_id;
CREATE TABLE IF NOT EXISTS likes (
    id integer PRIMARY KEY DEFAULT nextval('like_id') ,
    user_id integer references users(id),
    gif_id integer references gifs(id),
    CONSTRAINT ug UNIQUE (user_id, gif_id)
);
ALTER SEQUENCE like_id OWNED BY likes.id;

CREATE SEQUENCE  follower_id;
CREATE TABLE IF NOT EXISTS followers (
    id integer PRIMARY KEY DEFAULT nextval('follower_id') ,
    user_id integer references users(id),
    follower_id integer references users(id),
    CONSTRAINT uf UNIQUE (user_id, follower_id)
);
ALTER SEQUENCE follower_id OWNED BY followers.id;

CREATE SEQUENCE  tag_id;
CREATE TABLE IF NOT EXISTS tags (
    id integer PRIMARY KEY DEFAULT nextval('tag_id') ,
    user_id integer references users(id),
    gif_id integer references gifs(id),
    tag varchar(255)
);
ALTER SEQUENCE tag_id OWNED BY tags.id;