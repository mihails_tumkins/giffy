from tests import GiffyTestCase
from tornado import testing
from tornado import httpclient
import json
import requests

class UsersTestCase(GiffyTestCase):
    @testing.gen_test
    def test_user(self):
        user_one_data = { "username": self.random_name(),
                          "email": self.random_email(),
                          "password": self.random_password() }

        user_two_data = { "username": self.random_name(),
                          "email": self.random_email(),
                          "password": self.random_password() }

        payload_one = json.dumps(user_one_data)
        payload_two = json.dumps(user_two_data)

        #create users
        res_one = yield self.request('POST',
                                  'http://localhost:8080/users',
                                  payload_one,
                                  { "Content-Type":"application/json" })

        res_two = yield self.request('POST',
                                  'http://localhost:8080/users',
                                  payload_two,
                                  { "Content-Type":"application/json" })

        body_one = json.loads(res_one.body)
        body_two = json.loads(res_two.body)

        user_one_account_id = body_one["accountId"]
        user_one_access_token = body_one["accessToken"]

        user_two_account_id = body_two["accountId"]
        user_two_access_token = body_two["accessToken"]


        #upload gif by user one
        files = {'image': open('tests/data/bubbles.gif', 'rb')}
        headers = { "X-Giffy-AccessToken": user_one_access_token,
                    "X-Giffy-AccountId": user_one_account_id }
        data = {"description":"bubbles is #awesome"}

        gif_res = requests.post("http://localhost:8080/gifs", headers=headers, files=files, data=data)

        self.assertTrue(gif_res.status_code, 200)
        gif_resp = json.loads(gif_res.text)


        self.assertIsNotNone(gif_resp["imageId"])
        self.assertIsNotNone(gif_resp["filename"])

        gif_id = gif_resp["imageId"]

        #TODO
        #recommend gif for user two
        recommend_res = yield self.request('POST',
                                  'http://localhost:8080/recommend',
                                  {"user_id": user_two_account_id, "gif_id": gif_id },
                                  { "Content-Type":"application/json",
                                    "X-Giffy-AccessToken": user_one_access_token,
                                    "X-Giffy-AccountId": user_one_account_id })

        self.assertTrue(recommend_res.body, "OK")

        #get notifications for user two
        notification_res = yield self.request('GET',
                                  'http://localhost:8080/notifications',
                                  headers={ "Content-Type":"application/json",
                                    "X-Giffy-AccessToken": user_two_account_id,
                                    "X-Giffy-AccountId": user_two_access_token })

        self.assertTrue(self.is_json_response(notification_res))
        notification_body = json.loads(notification_res.body)

        self.assertIsNotNone(notification_body["notifications"])
        self.assertIsNotNone(len(notification_body["notifications"]), 1)

        notification = notification_body["notifications"][0]

        self.assertIsNotNone(notification["id"])
        self.assertTrue(notification["gif_id"], gif_id)
        self.assertNone(notification.get("seen"))

        #mark notification as seen
        post_notif_res = yield self.request('POST',
                                  'http://localhost:8080/notifications',
                                  {"notification_id": notification["id"] },
                                  { "Content-Type":"application/json",
                                    "X-Giffy-AccessToken": user_two_access_token,
                                    "X-Giffy-AccountId": user_two_account_id })
        self.assertTrue(recommend_res.body, "OK")

        #get notifications for user two
        notification_res = yield self.request('GET',
                                  'http://localhost:8080/notifications',
                                  headers={ "Content-Type":"application/json",
                                    "X-Giffy-AccessToken": user_two_account_id,
                                    "X-Giffy-AccountId": user_two_access_token })


        self.assertIsNotNone(notification_body["notifications"])
        self.assertIsNotNone(len(notification_body["notifications"]), 1)

        notification = notification_body["notifications"][0]

        self.assertIsNotNone(notification["id"])
        self.assertTrue(notification["gif_id"], gif_id)
        self.assertIsNotNone(notification["seen"])