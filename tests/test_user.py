from tests import GiffyTestCase
from tornado import testing
from tornado import httpclient
import json
class UsersTestCase(GiffyTestCase):
    @testing.gen_test
    def test_user(self):
        with self.assertRaises(httpclient.HTTPError):
            res = yield self.request('POST',
                                      'http://localhost:8080/users',
                                      '{}',
                                      { "Content-Type":"application/json" })

        data = { "username": self.random_name(),
                 "email": self.random_email(),
                 "password": self.random_password() }

        payload = json.dumps(data)

        #registration
        res = yield self.request('POST',
                                  'http://localhost:8080/users',
                                  payload,
                                  { "Content-Type":"application/json" })

        self.assertTrue(self.is_json_response(res))
        body = json.loads(res.body)

        self.assertIsNotNone(body["accountId"])
        self.assertIsNotNone(body["accessToken"])

        with self.assertRaises(httpclient.HTTPError):
            res = yield self.request('POST',
                                      'http://localhost:8080/users',
                                      payload,
                                      { "Content-Type":"application/json" })

        #login
        with self.assertRaises(httpclient.HTTPError):
            res = yield self.request('POST',
                                      'http://localhost:8080/users/login',
                                      json.dumps({ "username": data["username"],
                                                   "email": data["email"],
                                                   "password": "123123123" }),
                                      { "Content-Type":"application/json" })

        res = yield self.request('POST',
                                  'http://localhost:8080/users/login',
                                  payload,
                                  { "Content-Type":"application/json" })

        self.assertTrue(self.is_json_response(res))
        body = json.loads(res.body)

        self.assertIsNotNone(body["accountId"])
        self.assertIsNotNone(body["accessToken"])

        accountId = str(body["accountId"])
        accessToken = body["accessToken"]

        #user info
        with self.assertRaises(httpclient.HTTPError):
            res = yield self.request('GET',
                                     'http://localhost:8080/users/' + accountId,
                                     headers={ "Content-Type":"application/json" })

        res = yield self.request('GET',
                                 'http://localhost:8080/users/' + accountId,
                                 headers={ "Content-Type":"application/json",
                                           "X-Giffy-AccessToken" : accessToken,
                                           "X-Giffy-AccountId": accountId })

        self.assertTrue(self.is_json_response(res))
        body = json.loads(res.body)

        self.assertTrue(body["username"], data["username"])
        self.assertTrue(body["email"], data["email"])
        self.assertTrue(body["id"], accountId)


