from tornado.testing import AsyncTestCase
from tornado import gen, httpclient
import time
import random
import string

class GiffyTestCase(AsyncTestCase):

    def random_name(self, size=10):
        return "user_" + str(time.time()) + self.random_string()

    def random_email(self, size=10):
        return "user_" + str(time.time()) + self.random_string() + "@test.giffy.com"

    def random_password(self, size=10):
        return "pass_" + str(time.time())

    def random_string(self, size=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

    def is_json_response(self, response):
        return response.headers.get("Content-Type", "").startswith("application/json")

    @gen.coroutine
    def request(self, method, url, body=None, headers=None, follow_redirects=None, email=None, anon=False, password=None):
        _headers = headers or {}
        client = httpclient.AsyncHTTPClient(self.io_loop)

        request = httpclient.HTTPRequest(
            url,
            method=method,
            user_agent="Giffy/Giffy",
            headers=_headers,
            body=body,
            allow_nonstandard_methods=True,
            follow_redirects=follow_redirects,
            validate_cert=False,  # allow running tests locally
        )
        try:
            response = yield client.fetch(request)
        except httpclient.HTTPError as e:
            if e.code < 400:
                raise gen.Return(e)
            raise e
        raise gen.Return(response)