#!/usr/bin/env bash
# Vagrant box provisioning box to setup Python 2.7 development environment.
# See https://github.com/progrium/bashstyle for bash code style.
#
# Usage:
#   vagrant provision


main() {
  local project_dir="/vagrant"
  local virtualenv_dir="$project_dir/env"
  local user="giffy"
  local db="giffy"
  local password_hash="c6f7ca38-2ab9-11e5-b345-feff819cdc9f"
  local schemas="$project_dir/conf/schemas.sql"

  install_system_dependencies
  install_project_dependencies $project_dir $virtualenv_dir

  allow_postgresql_local_connection
  create_giffy_database $db $user $password_hash $schemas

  print_package_versions
  print_usage $project_dir $virtualenv_dir
}


install_system_dependencies() {
  # Some python packages depends on system libraries and their development
  # headers.
  apt-get update
  apt-get -y install build-essential python2.7-dev python-pip python-virtualenv \
    memcached postgresql postgresql-contrib postgresql-server-dev-9.3 libmagic1
}


install_project_dependencies() {
  declare project_dir="$1" virtualenv_dir="$2"

  virtualenv --clear "$virtualenv_dir"
  $virtualenv_dir/bin/pip install -r  "$project_dir/conf/requirements.txt"
}


allow_postgresql_local_connection() {
  # See http://www.depesz.com/2007/10/04/ident/
  service postgresql stop
  sed -i "s/local[ \t]*all[ \t]*all[ \t]*peer/local all all trust/" /etc/postgresql/9.3/main/pg_hba.conf
  service postgresql start
}


create_giffy_database() {
  declare db="$1" user="$2" password_hash="$3" schemas="$4"

  su postgres -c "createuser -DRS $user"
  su postgres -c "createdb $db"
  su postgres -c "psql -c \"GRANT ALL PRIVILEGES ON DATABASE $db to $user;\""
  su postgres -c "psql -c \"ALTER USER $user WITH ENCRYPTED PASSWORD '$password_hash';\""
  su postgres -c "psql $db $user -f \"$schemas\""
}


print_package_versions() {
  python --version
  echo "virtualenv $(virtualenv --version)"
  pip --version
  psql --version
}


print_usage() {
  declare project_dir="$1" virtualenv_dir="$2"

  echo "Use 'vagrant ssh' to connect to box"
  echo "Use 'vagrant ssh -c \"$virtualenv_dir/bin/python $project_dir/giffy.py --config=local\"'
    to run development server on http://localhost:8080"
}

main
