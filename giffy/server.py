import logging
log = logging.getLogger("giffy.server")

import tornado.web
import tornado.httpserver
import tornado.ioloop
from tornado.options import options
from tornado.web import URLSpec
from tornado import gen
import os

import giffy
from giffy.endpoints import ErrorHandler
from giffy.endpoints import user, gif, like, follow, feed, tag

import momoko
import asyncmc


class Application(tornado.web.Application):
    def __init__(self, config, env):
        handlers = [
            URLSpec(r'/static/(.*)', tornado.web.StaticFileHandler, {'path': "../../static"}),

            URLSpec(r"/following/([0-9]+)$", follow.FollowingHandler),
            URLSpec(r"/followers/([0-9]+)$", follow.FollowersHandler),
            URLSpec(r"/follow", follow.FollowHandler),
            URLSpec(r"/follow/([0-9]+)$", follow.FollowHandler),

            URLSpec(r"/likes", like.LikeHandler),
            URLSpec(r"/likes/([0-9]+)$", like.LikeHandler),

            URLSpec(r"/users/login", user.UserLoginHandler),
            URLSpec(r"/users", user.UsersHandler),
            URLSpec(r"/users/([0-9]+)$", user.UserHandler),

            URLSpec(r"/gifs", gif.GifsHandler),
            URLSpec(r"/gifs/([0-9]+)$", gif.GifHandler),

            URLSpec(r"/tag/([0-9]+)$", tag.TagHandler),

            URLSpec(r"/feed", feed.FeedHandler),
            URLSpec(r"/feed/following", feed.FeedFollowingHandler),
            URLSpec(r"/feed/user/([0-9]+)$", feed.FeedHandler),
            URLSpec(r"/feed/tag/(\w+)$", feed.FeedTagHandler),

            URLSpec(r"/.*", ErrorHandler, {"error_code": giffy.Error.NOT_FOUND})
        ]

        tornado.web.Application.__init__(
            self,
            handlers,
            debug=options.debug,
            static_path=os.path.join(os.path.dirname(__file__), "../static"),
        )

        self.config = config

        ioloop = tornado.ioloop.IOLoop.instance()
        self.mc = asyncmc.Client(servers=['%s' % config.mc_url], loop=ioloop)

        log.info("My hat is awesome!")


def run(config, env):
    ioloop = tornado.ioloop.IOLoop.instance()

    application = Application(config, env)
    application.db = momoko.Pool(
        dsn = 'dbname=%s user=%s password=%s host=%s port=%s' % (
            config.db_database, config.db_user, config.db_password, config.db_host, config.db_port),
        size=config.db_pool_size,
        ioloop=ioloop,
        raise_connect_errors=True
    )

    ioloop.add_future(application.db.connect(), lambda f: ioloop.stop())
    ioloop.start()

    http_server = tornado.httpserver.HTTPServer(application, xheaders=True)
    http_server.listen(options.port)
    ioloop.start()
