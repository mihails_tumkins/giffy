import tornado.web


class Error(tornado.web.HTTPError):

    UNKNOWN = 89999
    NOT_FOUND = 80000
    INVALID_JSON = 80001
    MISSING_PARAMETERS = 80002
    NOT_IMPLEMENTED = 80005
    UNAUTHORIZED = 80003
    BAD_PARAMETERS = 80004
    CONFLICT = 80006

    def __init__(self, error_code, error_message=None, status_code=None):
        self.error_code = error_code

        if not error_message:
            error_message = {
                Error.UNKNOWN: "Server error",
                Error.NOT_FOUND: "Not found",
                Error.INVALID_JSON: "Invalid JSON payload",
                Error.MISSING_PARAMETERS: "Missing parameters",
                Error.UNAUTHORIZED: "Unauthorized",
                Error.NOT_IMPLEMENTED: "Not implemented",
                Error.BAD_PARAMETERS: "Bad parameters",
                Error.CONFLICT: "Conflict",
            }.get(error_code, "Unknown Error")
        self.error_message = error_message

        if not status_code:
            status_code = {
                Error.UNKNOWN: 500,
                Error.NOT_FOUND: 404,
                Error.UNAUTHORIZED: 403,
                Error.NOT_IMPLEMENTED: 501,
                Error.CONFLICT: 412,
            }.get(error_code, 400)

        tornado.web.HTTPError.__init__(self, status_code=status_code)

    def __str__(self):
        return "Giffy Error %d: %s" % (self.error_code, self.error_message)
