import logging

import os
from tornado import gen

import giffy
from giffy.endpoints import AuthRequestHandler
from giffy import sugar as s
import magic
from psycopg2 import IntegrityError

class TagHandler(AuthRequestHandler):
    @gen.coroutine
    def get(self, id):
        id = s.str2int(id)
        gif = yield self.gif.get(id)
        if gif == None:
            raise giffy.Error(giffy.Error.NOT_FOUND, "Gif not found")
        tags = yield self.tag.get_by_gif_id(id)
        self.write_json(tags)