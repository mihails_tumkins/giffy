import logging

import os
from tornado import gen

import giffy
from giffy.endpoints import AuthRequestHandler
from giffy import sugar as s
import magic
from psycopg2 import IntegrityError

class FollowingHandler(AuthRequestHandler):
    @gen.coroutine
    def get(self, user_id):
        user_id = s.str2int(user_id)
        user = yield self.user.get(user_id)
        if user == None:
            raise giffy.Error(giffy.Error.NOT_FOUND, "User not found")

        following = yield self.follower.get_following_list(user_id)
        self.write_json(following or [])

class FollowersHandler(AuthRequestHandler):
    @gen.coroutine
    def get(self, user_id):
        user_id = s.str2int(user_id)
        user = yield self.user.get(user_id)
        if user == None:
            raise giffy.Error(giffy.Error.NOT_FOUND, "User not found")

        followers = yield self.follower.get_followers_list(user_id)
        self.write_json(followers or [])

class FollowHandler(AuthRequestHandler):
    @gen.coroutine
    def post(self):
        user_id = self.get_json_argument("user_id", None)
        follower_id = self.account_id

        if user_id == follower_id:
            raise giffy.Error(giffy.Error.BAD_PARAMETERS, "Can't follow yourself")

        user = yield self.user.get(user_id)
        if user == None:
            raise giffy.Error(giffy.Error.NOT_FOUND, "User not found")

        try:
            follower = yield self.follower.create(user_id, follower_id)
            self.write_json("OK")
        except IntegrityError:
            raise giffy.Error(giffy.Error.CONFLICT, "Already Following")

    @gen.coroutine
    def delete(self, user_id):
        user_id = s.str2int(user_id)
        follower_id = self.account_id
        yield self.follower.delete(user_id, follower_id)
        self.write_json("OK")