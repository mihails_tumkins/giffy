import logging

import os
from tornado import gen

import giffy
from giffy.endpoints import AuthRequestHandler
from giffy import sugar as s
import magic

class GifHandler(AuthRequestHandler):
    @gen.coroutine
    def get(self, id):
        id = s.str2int(id)
        gif = yield self.gif.get(id)
        if gif == None:
            raise giffy.Error(giffy.Error.NOT_FOUND, "Gif not found")
        self.write_json(gif)

class GifsHandler(AuthRequestHandler):
    @gen.coroutine
    def get(self):

        user_id = self.get_argument("user_id", None)
        if user_id:
            gifs = yield self.gif.get_by_user_id(user_id)
        else:
            gifs = yield self.gif.get_list()
        self.write_json(gifs)

        #left for testing purposes
        #form = """
        #    <!DOCTYPE html>
        #    <html>
        #    <body>
        #    <form action="/gifs" method="post" enctype="multipart/form-data">
        #        Select image to upload:
        #        <input type="file" name="image" id="image">
        #        <input type="hidden" name="accessToken" id="accessToken" value="{0}">
        #        <input type="hidden" name="accountId" id="accountId" value="{1}">
        #        <input type="text" name="description" id="description" value="">
        #        <input type="submit" value="Upload Image" name="submit">
        #    </form>
        #    </body>
        #    </html>
        #"""
        #res = form.format(self.access_token, self.account_id)
        #self.write(res)

    @gen.coroutine
    def post(self):

        description = self.get_argument("description", None)
        try:
            image = self.request.files['image'][0]
        except:
            image = None

        if not image:
            raise giffy.Error(giffy.Error.MISSING_PARAMETERS, "No image provided")


        if not description:
            raise giffy.Error(giffy.Error.MISSING_PARAMETERS, "No description prodided")


        image_data = magic.from_buffer(image['body'])

        filename = image['filename']
        extension = os.path.splitext(filename)[1]

        if "GIF image data" in image_data:
            extension = ".gif"

        if extension.lower() != ".gif":
            raise giffy.Error(giffy.Error.BAD_PARAMETERS, "File extension must be gif")


        filename = s.random_image_name() + extension.lower()
        output_file = open("static/uploads/" + filename, 'w')
        output_file.write(image['body'])


        image_id = yield self.gif.create(self.account_id, filename, description)


        tags = list({tag.strip("#") for tag in description.split() if tag.startswith("#")})
        for tag in tags:
            self.tag.create( image_id, self.account_id, tag)

        self.write_json({
            "imageId": image_id,
            "filename": filename
        })