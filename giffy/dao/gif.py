import logging

from tornado import gen
import momoko

class GifDAO(object):
    def __init__(self, db):
        self.db = db

    @gen.coroutine
    def get(self, id=0):
        sql = """
            SELECT id, user_id, filename, description
            FROM gifs
            WHERE id=%s
        """
        cursor = yield self.db.execute(sql, (id,))
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        if len(result) == 0:
            result = None
        else:
            result = result[0]
        raise gen.Return(result)

    @gen.coroutine
    def get_by_filename(self, filename):
        sql = """
            SELECT id, user_id, filename, description
            FROM gifs
            WHERE filename=%s
        """
        cursor = yield self.db.execute(sql, (filename,))
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        if len(result) == 0:
            result = None
        else:
            result = result[0]
        raise gen.Return(result)

    @gen.coroutine
    def get_by_user_id(self, user_id):
        sql = """
            SELECT id, filename, description
            FROM gifs
            WHERE user_id=%s
        """
        cursor = yield self.db.execute(sql, (user_id,))
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        raise gen.Return(result)

    @gen.coroutine
    def get_from_following_by_user_id(self, user_id):
        sql = """
            SELECT id, filename, description
            FROM gifs where user_id IN(
                SELECT user_id
                FROM followers
                WHERE follower_id=%s)
        """
        cursor = yield self.db.execute(sql, (user_id,))
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        raise gen.Return(result)

    @gen.coroutine
    def get_by_tag(self, tag):
        sql = """
            SELECT id, filename, description
            FROM gifs where id IN(
                SELECT DISTINCT gif_id
                FROM tags
                WHERE tag=%s)
        """
        cursor = yield self.db.execute(sql, (tag,))
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        raise gen.Return(result)

    @gen.coroutine
    def get_list(self):
        sql = """
            SELECT id, user_id, filename, description
            FROM gifs
        """
        cursor = yield self.db.execute(sql)
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        raise gen.Return(result)

    @gen.coroutine
    def create(self, user_id, filename, description=None):
        sql = """
            INSERT INTO gifs (user_id, filename, description)
            VALUES (%s, %s, %s  )
            RETURNING *;
        """
        cursor = yield self.db.execute(sql, (user_id, filename, description))
        row_id = cursor.fetchone()[0]
        cursor.close()
        raise gen.Return(row_id)

    @gen.coroutine
    def update(self, id, data={}):
        fields = ''
        for key in data.keys():
            fields += '{0}=%s,'.format(key)

        sql = """
            UPDATE gifs
            SET {0}
            WHERE id=%s
        """.format(fields[0:-1])
        params = list(data.values())
        params.append(id)
        cursor = yield self.db.execute(sql, params)
        cursor.close()
        raise gen.Return(cursor)

    @gen.coroutine
    def delete(self, id):
        sql = """
            DELETE
            FROM gifs
            WHERE id=%s
        """
        cursor = yield self.db.execute(sql, (id,))
        cursor.close()
        raise gen.Return(cursor)
